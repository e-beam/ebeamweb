# Working Group Meetings List
List of future and previous working group meetings with link to the presentations and minutes.
An **action list** is also maintained **[here](minutes/actionList.md)**.

## Tentative List for Future Topics

### Electron cooling 
- Follow up on AD Cooler Consolidation 
    - Influence of AD optics on cooler performance (D.Gamba, G.Tranquille)
    - Comparison between present and future AD cooler: gains in cycle and beam performance (D.Gamba, A.Latina, L.Ponce)
        - Reasons for the present degraded cooling/cycle time
        - Emittance evolution during last 20 years  
- ELENA Machine Status (C. Carli, D. Gamba, L.Ponce)
    - Electron cooling with H- in ELENA 
- Simulation of e-cooling (A. Borucka, D.Gamba, A. Latina, N. Biancacci, N. Madysa, V. Kain) 
    - Follow up on trying using reinforcement learning to ease e-cooler setup
    - Use RF-Track+BetaCool to simulate/predict expected performance of various coolers
- Follow up on HW intervention on e-coolers (G. Tranquille, A. Frassier)
    - Schottky, BPM, AD collectors, ... 
- E-cooling physics (BE-ABP + SY-BI + BE-OP)
    - Review theoretical aspects 

### Electron lenses
- Status of HEL design 
    - Optimization of HEL Collector (A.Rossi, D.Nikiforov)
        - Dump efficiency
    - Simulation of e- beam from gun to collector
    	- Injected/extracted e- beam: e- orbit "bump" issue solved?
    - Instrumentation "review": do we have enough instrumentation? 
        - Backscattered electron Monitor (No people currently working on this)
        - Modulation of e- current to measure e- orbit
    - HEL specifications verification:
        - total residual kick to circulating beam < 1 nrad: how to define tolerances on electron beam?
- HEL Test Stand Results (A.Rossi, S. Sadovich)
    - Results from test with BGC
    - Performance of on the *new* old gun
    - Simulation of time evolution of electron beam when on/off (medium term)
        - Can this be used to monitor DC part of the beam?

### Other topics
- CERN E-Cooler Test Stands
    - Characteristics and parameters, results and future plans (G.Tranquille, J.Cenede)

## Previous Meetings
- **20/06/2022**: E-Beam - #15 Working Group Meeting  - [indico](https://indico.cern.ch/event/1161361/) -[minutes](minutes/2022-06-20.md)
    - Electron Beam Tests Stand: experimental results and e-beam simulations *(Sameed Muhammed)*

- **24/11/2021**: E-Beam - #14 Remote Working Group Meeting  - [indico](https://indico.cern.ch/event/1097370/) -[minutes](minutes/2021-11-24.md)
    - New AD e-cooler particle tracking in Opera *(Luke Von Freeden)*

- **13/10/2021**: E-Beam - #13 Remote Working Group Meeting  - [indico](https://indico.cern.ch/event/1048496/) -[minutes](minutes/2021-10-13.md)
    - Status of the HEL optics for the e-beam *(Adriana Rossi)*
    - Injection of the e-beam injection around the LHC beam: issues and possible solutions *(Danila Nikiforov)*

- **07/09/2021**: E-Beam - #12 Remote Working Group Meeting  - [indico](https://indico.cern.ch/event/1071627/) -[minutes](minutes/2021-09-07.md)
    - Status of e-cooling simulations with Betacool and RF-Track *(Agnieszka Elzbieta Borucka)*

- **12/05/2021**: E-Beam - #11 Remote Working Group Meeting - [indico](https://indico.cern.ch/event/1025194/) - [minutes](minutes/2021-05-12.md)
    - New AD e-cooler project status *(Gerard Alain Tranquille)*
    - On possible AD optics improvements *(Davide Gamba)*
    
- **21/04/2021**: E-Beam - #10 Remote Working Group Meeting - [indico](https://indico.cern.ch/event/1021517/) - [minutes](minutes/2021-04-21.md)
    - Bunch compression studies for inner target collisions within ELENA ring *(Volodymyr Rodin)*

- **26/03/2021**: ColUSM #139 - [indico](https://indico.cern.ch/event/1021048/)
    - Tolerances on pulse-to-pulse noise of the electron beam *(D. Mirarchi)*
    - Update on specification for the HEL *(S. Redaelli)*
    - Update on specification for the HEL modulator *(A. Rossi)*

- **09/02/2021**: Special Joint LIU-ions meeting - [indico](https://indico.cern.ch/event/1000607/)
    - Cooling force comparison among different models in RF-Track, Betacool and JSPEC *(Agnieszka Borucka)*

- **15/09/2020**: Special Joint WP2/WP5 meeting - [indico](https://indico.cern.ch/event/945851/)
    - Halo removal and effects on beam core of electron lens in HL-LHC *(Daniele Mirarchi)*

- **09/09/2020**: E-Beam - #7 Remote Working Group Meeting - [indico](https://indico.cern.ch/event/949138/) - [minutes](minutes/2020-09-09.md)
    - Status of Reinforcement Learning Studies for E-Cooler Operation *(Nico Madysa)*

- **08/07/2020**: E-Beam - #6 Remote Working Group Meeting - [indico](https://indico.cern.ch/event/935417/) - [minutes](minutes/2020-07-08.md)
    - Warp Simulation of Electron Transport in HEL *(Ondrej Sedlacek)*
    - CST PARTICLE STUDIO Mesh variation and impact on magnetic field and beam trajectory simulations *(Ana Miarnau Marin)*
    - A.O.B. Debye Length in a Magnetised Non-Neutral Plasma

- **20/05/2020**: E-Beam - #5 Remote Working Group Meeting - [indico](https://indico.cern.ch/event/915406/) - [minutes](minutes/2020-05-20.md)
    - Status on e-cooling simulations with BETACOOL and RF-Track *(B. Veglia)*

- **29/04/2020**: E-Beam - #4 Remote Working Group Meeting - [indico](https://indico.cern.ch/event/910625/) - [minutes](minutes/2020-04-29.md)
    - Simulation of electron transport within Hollow Electron Lens using Warp – Status report *(O. Sedlacek)*

- **08/04/2020**: E-Beam - #3 Remote Working Group Meeting - [indico](https://indico.cern.ch/event/904710/) - [minutes](minutes/2020-04-08.md)
    - e- beam orbit optimisation in ELENA *B. Galante*
    - ELENA e-cooler electrostatic and magnetostatic settings *G.A. Tranquille*

- **01/04/2020**: E-Beam - #2 Remote Working Group Meeting - [indico](https://indico.cern.ch/event/904680/) - [minutes](minutes/2020-04-01.md)
    - Status of SixTrack implementation for Hollow Electron Lens *A. Mereghetti*

- **25/03/2020**: E-Beam - #1 Remote Working Group Meeting - [indico](https://indico.cern.ch/event/886200/) - [minutes](minutes/2020-03-25.md)
    - CST simulations of the inlet arm of the Hollow Electron Lens *A. Rossi*

- **15/01/2020**: E-Beam - #8 Working Group Meeting - [indico](https://indico.cern.ch/event/870565/) - [minutes](minutes/2020-01-15.md)
    - Diagnostics of BNL e-lenses *A. Pikin*
    - Beam-Gas Curtain (BGC) monitor for the Hollow e-lens *R. Veness* 
    - Status of e- beam orbit measurement in ELENA e-cooler *D. Gamba* 
    - BPMs for e-coolers and e-lenses *L. Soby*
    - Betacool for UNIX *D. Gamba*

- **06/11/2019**: E-Beam - #7 Working Group Meeting - [indico](https://indico.cern.ch/event/860133/) - [minutes](minutes/2019-11-06.md)
    - Simulation of e- beam with WARP *O. Sedlacek*
    - E-Lens test stand *S. Sadovich* 

- **18/09/2019**: E-Beam - #6 Working Group Meeting - [indico](https://indico.cern.ch/event/846234/) - [minutes](minutes/2019-09-18.md)
    - AD/ELENA e-cooler requirements, and past and present performances, *D. Gamba*
    - Status and plans for hardware interventions on AD/ELENA e-cooler during LS2, *G. A. Tranquille*

- **10/07/2019**: E-Beam - #5 Working Group Meeting - [indico](https://indico.cern.ch/event/829076/) - [minutes](minutes/2019-07-10.md)
    - LEIR e-cooler operational experience and needs, *A. Saa Hernandez, N. Biancacci*
    - Status and plans for hardware interventions on LEIR e-cooler, *G. A. Tranquille*
    - AoB: LEIR BI Schottky System Status, *O. Marqversen*
    - AoB: Future Topics, *A. Rossi*

- **29/05/2019**: E-Beam - #4 Working Group Meeting - [indico](https://indico.cern.ch/event/823622/)
    - E-beam simulation with latest geometry of HEL, *D. Nikiforov*

- **10/05/2019**: ColUSM#116 - E-BEAM joint meeting - [indico](https://indico.cern.ch/event/816028/) - [minutes](https://indico.cern.ch/event/816028/attachments/1852734/3042130/10052019_ColUSM116.pdf)
    - Powering scheme for HEL, *D. Mirarchi*
    - Design of HEL collector, *G. Gobbi*
    - Update on IR7 layout for a collimation system with 8 crystals, *M. D'Andrea*

- **13/03/2019**: E-Beam - #2 Working Group Meeting - [indico](https://indico.cern.ch/event/802502/) - [minutes](minutes/2019-03-13.md)
    - E-Beam: mandate and organisational matters, *A. Rossi*
    - E-cooling measurements at LEIR, *Angela Saa Hernandez*
    - Simulation and code development plans for LS2, *.A. Latina*

- **10/01/2019**: E-coolers and e-lenses simulations - joint BE-ABP / BE-BI meeting - [indico](https://indico.cern.ch/event/774322/)
    - Past and present of electron cooling at CERN (AD, LEIR, ELENA …), *G.A. Tranquille*
    - Simulations of e-gun for AD e-cooler, *A. Pikin*
    - Simulations of e-cooling effect at LEIR/ELENA, *A. Latina*
    - Operational aspects and diagnostics of AD and ELENA e-coolers, *L.V. Joergensen*
    - Experience with e-cooler at ELENA and requirements for optimisation of future applications, *C. Carli, D. Gamba*
    - Summary of e-beam studies for the HL-LHC Hollow Electron Lens, *A. Rossi*
    - Simulations for e-lens test stand guns and beams, *S. Sadovich*
    - Effect of e-lens on circulating beams, *D. Mirarchi*
    - What next?, *A. Rossi*


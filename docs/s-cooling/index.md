# S-Cooling

Stochastic cooling is a complementary cooling mechanism used in AD to cool antiproton beams at 3.575 GeV/c and 2 GeV/c

!!!info "S-cooling at CERN"
    - **2021:** _W. Hofle_, Recommissioning of the CERN AD Stochastic Cooling System in 2021 after Long Shutdown 2 - presentation at COOL2021 [link](https://indico.inp.nsk.su/event/59/attachments/1468/1976/S302_Talk.pdf) - [proceedings](https://epaper.kek.jp/cool2021/papers/s302.pdf)


!!!info "General References on s-cooling"
    - **2016:** Stockhorst, H. et al. Beam Cooling at COSY and HESR Theory and Simulation – Part 1 Theory [link](http://hdl.handle.net/2128/10160)
        - Nice overview of theory of S-cooling, with hands-on suggestions of what should measure in real life. 
    - **2012:** F. Caspers and D. M\"ohl, History of stochastic beam cooling and its application in many different projects [EPJ H 36, 601–632 (2012)](https://doi.org/10.1140/epjh/e2012-20037-8)
        - History of Stochastic cooling with references to most important papers
    - **2003:** M. Blaskiewicz, Stochastic Cooling Studies in RHIC [PAC2003](https://www.bnl.gov/isd/documents/25501.pdf)
        - Conference proceeding with some useful formula
    - **2000:** _C. Carli and F. Caspers_, STOCHASTIC COOLING AT THE CERN ANTIPROTON DECELERATOR, [EPAC2000](https://accelconf.web.cern.ch/e00/PAPERS/THP1B08.pdf)
        - Conference proceedings with achieved performance of s-cooling and some observations (e.g. longit. signal in transverse BTF)
    - **1984:** S. van der Meer, Stochastic Cooling and the Accumulation of Antiprotons [noble lecture](https://www.nobelprize.org/uploads/2018/06/meer-lecture.pdf)
        - Description of cooling by its inventor for antiproton cooling in AA
    - **1983:** C.S. Taylor, Stochastic Cooling Hardware [CAS1983](https://inspirehep.net/literature/197803)
        - Description of main concepts, hardware needed, and possible faults and solutions
    - **1978:** F. Sacherer, Stochastic Cooling Theory [inspire](https://inspirehep.net/literature/130674)
        - Reference paper with details on theory of s-cooling using Fokker-Planch equation

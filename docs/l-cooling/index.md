# L-Cooling

Laser cooling is being proposed as mean to cooling highly relativistic ion beams.


!!!info "General References on l-cooling"
    - **2020:** M. W. Krasny et al., High-luminosity Large Hadron Collider with laser-cooled isoscalar ion beams [arXiv:2003.11407](https://arxiv.org/pdf/2003.11407.pdf)
        - Description of a proposal for a isoscalar ion collider in the LHC, including laser-cooling related aspects.
    - **2019:** Martens , Aurelien, Gamma Factory Proof-of-Principle Experiment (https://cds.cern.ch/record/2690736)
        - Details of the PoP Experiment in the SPS
    - **2015:** A. Petrenko, Ion-photon interaction kinematics and laser cooling of ion in a storage ring [python notebook](https://www.inp.nsk.su/~petrenko/misc/ion_cooling/animations/)

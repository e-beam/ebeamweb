# Action list

!!! danger "Open Actions"
     - [12/05/2021](2021-05-12.md) **BI**: Verify that design of new AD cooler BPMs is on track.
     - [08/04/2020](2020-04-08.md) **D. Gamba**: check with G. Tranquille about available public documentation on the ELENA cooler modeling in COMSOL. Also check if a "compiled" version of the COMSOL model can be made available.
     - [15/01/2020](2020-01-15.md) **G. Tranquille, A. Frassier**: implement hardware needed for $e^-$ beam intensity modulation in all e-coolers.
     - [15/01/2020](2020-01-15.md) **L. Soby**: finalize implementation of acquisition chain for e- beam orbit in all e-coolers.
     - [15/01/2020](2020-01-15.md) **L. Soby**: investigated on charging-up effect of e-cooler BPMs.
     - [15/01/2020](2020-01-15.md) **D. Gamba, L. Soby**: verify if using the RF frequency as $e^-$ intensity modulation is the best option.
     - [15/01/2020](2020-01-15.md) **c, L. Soby**: investigate if a lock-in acquisition system for e-cooler BPMs could be useful.
     - [15/01/2020](2020-01-15.md) **L. Soby**: provide an estimate of BPM resolution in the different systems.
     - [06/11/2019](2019-11-06.md) **D. Gamba**: Verify with A. Latina if RF-Track model of e-cooler (and/or e-lenses) dynamics is possible and of any use.
     - [10/07/2019](2019-07-10.md) **A. Rossi**: Set up a framework for simulating the e- beam in the e-cooler
     - [10/07/2019](2019-07-10.md) **D. Gamba, A. Latina**: Set up a framework for simulating the electron-ion interaction in LEIR
     - [10/07/2019](2019-07-10.md) **S. Hirlaender**: Explore the possibility of using reinforced learning algorithms for e-cooler optimization (in collaboration with A. Latina, D. Gamba)
     - [10/07/2019](2019-07-10.md) **N. Biancacci, R. Scrivens**: Study the possibility of optics coupling compensation at startup (July 2021?)


!!! success "Closed Actions"
     - [12/05/2021](2021-05-12.md) **D. Gamba**: check the needs for horizontal/vertical orbit corrector strength for new AD cooler.
        - See presentation at [AD e-cooler review](https://indico.cern.ch/event/1175420/)
     - [18/09/2019](2019-09-18.md) **D. Gamba**: prepare "official" optics (Twiss files) of AD for cooling efficiency studies.
        - See official optics repositories of [AD](https://acc-models.web.cern.ch/acc-models/ad/) 
     - [18/09/2019](2019-09-18.md) **D. Gamba**: provide equations to implement LEIR e-cooler setup via higher level parameters (e.g. $e^-$ beam velocities, if possible)
        - See LIU-Ions PS-Injectors 3/11/20 on [indico](https://indico.cern.ch/event/971104/)
     - [10/07/2019](2019-07-10.md) **D. Gamba, L. Ponce**: organize follow-up AD optics studies following ideas of Pavel Belochitskii
        - See E-BEAM presentation on 12 May 2021 on [minutes](2021-05-12.md)
     - [10/07/2019](2019-07-10.md) **N. Biancacci, D. Gamba**: Prepare e-cooler lecture for LEIR operators
        - Presented on 19 Nov 2020, see [indico](https://indico.cern.ch/event/961114/)
     - [10/07/2019](2019-07-10.md) **R.A. Fernandez**: Coordinate the consolidation of the LEIR e-cooler software control system for operation, especially the set-up of high level parameters in collaboration with BI and the verification of unknown knobs (e.g. LEIRBEAM/DX_EC and LEIRBEAM/BeCool)
        - Discussed at LIU-Ions working group [indico](https://indico.cern.ch/category/8033/)
     - [10/07/2019](2019-07-10.md) **A. Frassier**: Follow-up on the fast switch implementation for e- gun gird/control electrodes (tests in LEIR to be coordinated with OP)
        - Decided to use modulation of electron intensity - to be checked with beam in 2021
     - [10/07/2019](2019-07-10.md) **G. Tranquille**: Installation of coupling transformer for grid electrode modulation in LEIR
        - Done by Alexandre Frassier. To be checked with beam.
     - [10/05/2019](2019-05-10.md) **ColUSM**: refine HEL specs in terms of pulse length, repetition rate, rise time, stability, bandwidth.
        - See EDMS [#2514085](https://edms.cern.ch/document/2514085/LAST_RELEASED)
     - [06/11/2019](2019-11-06.md) **A. Frassier, D. Gamba**: understand why the head amplifiers of AD e-cooler have been removed.
        - Being followed up by L. Soby (See E-BEAM #8 [15/01/2020](2020-01-15.md))
     - [18/09/2019](2019-09-18.md) **G. Tranquille**: follow up on e-beam orbit measurement system in all coolers.
        - Being followed up by G. Tranquille and A. Frassier (See E-BEAM #8 [15/01/2020](2020-01-15.md))
     - [10/07/2019](2019-07-10.md) **D. Gamba**: Circulate link to the new [E-Beam website](https://e-beam.web.cern.ch)
     - [13/03/2019](2019-03-13.md) **A. Saa Hernandez and others from LEIR team**:  Further analyze LEIR data to understand if space-charge or IBS dominated emittance growth.
        - Discussed at space charge workshop [indico](https://indico.cern.ch/event/828559/contributions/3528401/attachments/1939984/3216621/SC2019_Saa.pdf)
     - [13/03/2019](2019-03-13.md) **D. Gamba**: Organise doodle to choose date/time compatible with most participant constraints.
        - Wednesdays in the morning.

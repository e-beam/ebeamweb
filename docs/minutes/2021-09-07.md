# Minutes of #13 Remote E-Beam Meeting (draft)
The meeting was held on [Zoom](https://cern.zoom.us/j/62092233055?pwd=NElXM3hyWVBONlk0WUY3SnBxZVhCUT09) on 07/09/2021 - See [indico](https://indico.cern.ch/event/1071627/)


!!! info "Participants"
    Nicolo Biancacci, Agnieszka Borucka, Jean Cenede, Roberto Corsini, Reyes Alemany Fernandez, Alexander Frassier, Davide Gamba, Lars Joergensen, Alexander Pikin, Adriana Rossi, Gerhard Schneider, Fredrik Wenander, Michail Zampetakis

---

## Status of e-cooling simulations with Betacool and RF-Track *(Agnieszka Borucka)*

Agnieszka gave an overview of the latest comparisons between [Betacool](https://gitlab.cern.ch/e-beam/betacool/) and [RF-Track](https://gitlab.cern.ch/rf-track/rf-track-2.0).
In previous studies (See presentation at [LIU-ions 9Feb2021](https://indico.cern.ch/event/1000607/)), the modelled force on a single ion for different models was presented, and considerable differences were shown. 
Agnieszka's new studies are now concentrated on the emittance/energy spread evolution in time for different models and beam parameters.
To be stressed that no heating effects like Intra Beam Scattering or Space Charge are being considered at the moment.

The Betacool Toepffer's model is mathematically closer to the Nersisyan's model implemented in RF-Track.
Unfortunately, when applied to real tracking cases Toepffer's model seems to diverge, giving nonphysical results. 
Attempt to understand the reason of this divergency were made, but those did not come to a conclusion. This model was therefore discarded, and finally the study is now concentrated in comparing results between Parkhomchuk's (Betacool) and Nersisyan's (RF-Track) models.

Despite the cooling force on a single ion was observed to be considerably different among different codes/models, the evolution of emittance was found to be similar between codes/models (at least the ones that was showing cooling...), but cooling times were found to be about a factor 2 faster (or more) for RF-Track than Betacool.
This could be due to different estimation of the force for high delta velocities.

The Nersisyan's model presented some ambiguities in the Magnetised component of the force, which were "discovered" during the implementation in RF-Track.
Agnieszka compared three different implementation of RF-Track, and made comparions of cooling behavior as a function of different parameters.
Scan over transverse electron temperature allowed to discard the *RF-Track A* implementation, while no strong differences were observed between *RF-Track B* and *RF-Track C*. Finally, *RF-Track C* was choosen to be the only model implemented, as this showed the best robustness against extreme parameter (e.g. very low electron temperatures).

A striking difference between RF-Track with Betacool (Parkhomchuk) was observed when scanning over magnetic field in the e-cooler, i.e. directly affecting the magnetised component of the cooling force. 
This could be a possible experiment to be performed on an e-cooler, to allow for choosing between the two models.
Note that the impact of this solenoidal field on the circulating ion was neglected for both RF-Track and Betacool.

Agnieszka also studied the impact of space charge on the electron beam on the cooling process. Those studies were mainly performed using RF-Track, after validating that the space charge effect implemented in both codes was giving the same results.
In simulation, it was possible to see the raise of a sharp edge in the ion beam momentum spectra due to the electron velocity distribution coupled with dispersion in the circulating beam. 
This effect could be seen during the recent re-commissionig of the AD e-cooling at 300 MeV/c, and agrees qualitatively with the simulations. 

Finally, Agnieszka tried to reproduce old LEAR data of cooling performance as a function of Twiss parameters (See [CERN-PS-99-033-DI](http://cds.cern.ch/record/388111)), and still considering electron beam space charge effects. 
Results show that the general behavior can be reproduced, but cooling times observed in LEAR are considerably longer in simulation than in the experimental data. In this respect, RF-Track seems to better match reality.
To be noted that by changing the definition of cooling time, or the electron beam temperatures one can seriously affect the final result of the simulation.


### Discussion
**Reyes** asked if this study will call for MDs in LEIR. **Davide** mentioned that at the moment the simulation results cannot be quantities compared with data due to still too many uncertainties on the knowledge of e-cooler paramters, and the absence of heating effects in the present simulation.

**Nicolo** commented that adding space charge effects should be easily done, as this was already done in previous studies. 
This should have the highest priority, such to understand, at least in simulation, what could be the impact of those effects and indirectly constrain e-cooler parameters for future simulation studies.
Iterations to have IBS model. **Michail** commented that at the moment his implementation can be used only for machines above transition, but probably it could be extended also to our low energy cases: **to be continued!**

**Lars** suggested to try retrieve data also from e-coolers outside CERN: it is possible that scans over different parameters have been done already in other facilities.

---

## AoB: Plans fo future E-BEAM meetings

Adriana informed that the Hollow Electron Lens team is working on several topics.
Of particular interest is the simulation of the trajectory and stability of the electron beam orbit inside the electron lens. An E-BEAM session on this topic might be organised soon.

Additionally, we should come back to discuss the status of the new AD e-cooler electron collector.







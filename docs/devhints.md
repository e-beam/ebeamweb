# About this website development

This website is based on [MkDocs](https://www.mkdocs.org) and [Markdown](https://3os.org/markdownCheatSheet/welcome/).

- Find below the instructions has been followed to set up the present website.
- On the left menu you can find a few links to interesting resources.


## Setup of MkDocs

> 1. Ask to create a PaaS official website via https://webservices.web.cern.ch/webservices/ (suggestion: make it public on the internet)
> 3. Create a simple public gitlab project (https://gitlab.cern.ch/dgamba/ebeamweb)
> 4. From the [openshift administration tool](https://openshift.cern.ch/console/project/e-beam/catalog) of the PaaS website, create a MkDocs application, linked to the above gitlab project (*do not forget the .git at the end*)
> 5. basically, that's it! Start editing your gitlab project .md files!

### Adding webhook
This allows trigger a re-build of the website after a "push" on the gitlab repository:

> 1. in "builds" configuration look for the right url
> 2![](img/webhook-paas.png)
> 3. paste into your gitlab configuration
> ![](img/webhook-gitlab.png)

### Adding support for latex

> 1. activate mymdownx.arithmatex extension that is available by default in mkdocs container provided by CERN
> 2. I sugesst also to activate admonition extension (and eventually other pymdownx extensions) for a nicer website.
> 3. All this is done by adding the following lines in your mkdocs.yml configuration file:
>
> ```
> markdown_extensions:
>     - pymdownx.arithmatex
>     - admonition
> 
> extra_javascript:
>     - 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML'
> ```
>
> !!! warning
>     if you want to add **local** MathJs, then you need to:
>
>     1. download to your repository the latest version of MathJax.
>     ``
>     git clone https://github.com/mathjax/MathJax.git
>     ``
>     2. add everything inside your *docs* folder, e.g. under
>     ``
>     docs/js/MathJax-2.7.5
>     ``
>     3. replace the *extra_javascript:* configuration as following:
>     ```
>     extra_javascript:
>         - 'js/MathJax-2.7.5/MathJax.js?config=TeX-MML-AM_CHTML'
>     ```


### Using different Git branch
>
> 1. Edit your "build" YAML configuration file on openshift:
> ![](img/buildConfigFile.png)


### Run it as local server

It is very unconvenient to edit your pages on gitlab website or even locally, then pusshing them to git, and let the OpenShift server recompile the website for you. 
Better you:

> 1. clone your git repository locally
> 2. run mkdocs locally
>
>     1. ``pip install mkdocs ``
>     2. ``pip install mkdocs-material``
>     3. ``mkdocs serve`` (to be executed inside your local git repository)
>
> 3. connect your local website: http://127.0.0.1:8000



## Move your website to new infrustructure (Sep 2021)

Compact version of instruction found in [official doc](https://how-to.docs.cern.ch/gitlabpagessite/migration/)

1. Delete your "host" from `https://webservices-portal.web.cern.ch/my-sites'. 
2. ==Wait for confirmation!== for host deletion
3. Create new project https://webeos.cern.ch/ in `Administrator' mode
4. Under Operators->Installed Operators; click on `GitLab Pages Site Operator'
5. Click on `Publish a static site from a GitLab repository (gitlab.cern.ch)' **Create instance**
6. Fill the form, importat to specify as host the actual old host (or a new one! ;)), e.g. `e-beam.web.cern.ch', make it anonymous (to be freely accessible), and "Create".

At this point, edit your gitlab repository hosting the markdown files...:

0. Add `.gitlab-ci.yml' from this [example](https://gitlab.cern.ch/authoring/documentation/mkdocs-container-example)
1. Settings->pages: disable `Force HTTPS (requires valid certificates)' and **Save**.
2. Create a `New domain' specifying the usual host (e.g. `e-beam.web.cern.ch')
3. Cleanup old webhook...

# Test page

## Playing with logos
### Concept of e-cooling and e-lenses
![](img/e-beamLogo.png)

### Concept of e-cooling
![](img/e-coolLogo.png)

## Using formulas
only forumla:
$$ x + \frac{y}{4} = 6^2 $$

inline formula: $x/2 + y = \sqrt{6}$

## Some other feature
### subscript
H<sub>2</sub>O + 1
### superscript
Hello<sup>this is superscript</sup>

# Extra Low ENergy Antiproton ring (ELENA)

## The decelerator

!!!info "References"
     - 2014: ELENA Design Report [CERN-2014-002](https://cds.cern.ch/record/1694484/files/CERN-2014-002.pdf)
     - [ELENA Wikis](https://wikis.cern.ch/display/ELENAOP/) - accessible only within CERN

## The e-cooler
![Screenshot](img/ELENAcooler.jpg){: style="max-width: 600px"}

### Cooler Parameter List
|                               |   Intermediate plateau    |  Last plateau   |
| ----------------------------- |:-----------------------:  |:---------------:| 
| pbar momentum (MeV/c)         | 35                        | 13.7            |
| $\beta$                       | 0.037	                    | 0.015           |
| $e^-$ beam energy (eV)        | 355                       | 55              | 
| $e^-$ current (mA)            | 5                         | 1               |
| $e^-$ beam density ($m^{-3}$) | 1.38 x $10^{12}$          | 1.41 x $10^{12}$ |
| $B_{gun}$ (G)                 | 1000                                      ||
| $B_{drift}$ (G)               | 100                                       ||
| Toroid bending radius (m)	    | 0.25                                     ||
| Total cooler length (m)	    | 2.33                                     ||
| Expansion factor              | 10                                       ||
| Cathode radius (mm)           | 8                                        ||
| Electron beam radius (mm)     | 25                                       ||
| Twiss parameters (m)          | $\beta_x$=2.1, $\beta_y$=2.2, $D_x$=1.5 ||

### Powering scheme
Detailed electrical schematics available [here](docs/Power_Diagram_ELENA.pdf) (by A. Frassier)    

### Magnetic system
The magnetic system of the ELENA cooler is rather complex due to the presence of several correcition coils.
Some measurement of the field are available in this local document [here](docs/ELENA_MeasReport_17_05_26.pdf), while simulation of the different coils is on EDMS [#1684925](https://edms.cern.ch/document/1684925/0.1).

Assuming EDMS [#1684925](https://edms.cern.ch/document/1684925/0.1) to be correct, here some interesting values:

|  Parameter                      |   value  |
| ---------------------- |:-----------------------:|
| Expansion solenoid length (mm)        | 550             | 
| Expansion solenoid field (gauss)      | 1000 (max 2500) | 
| Expansion solenoid current (A)        | 137 (max ???)   | 
| Drift solenoid length (mm)            | 1000            |
| Drift solenoid field (gauss)          | 100 (max 250)   | 
| Drift solenoid current (A)            | 41.35 (max ???) | 
| Toroid bending radius (mm)            | 250             |
| Toroid bending angle (deg)            | 90              |
| Toroid arc length (mm)                | 392.7           |
| Toroid field (gauss)                  | 100 (max 250)   |
| Toroid current (A)                    | 28 (max ??)     |
| Squeeze solenoid current (A)          | 5.17            | 
| Squeeze solenoid field (Gauss)        | 15 (max 50)     | 



### Technical drawings
The Drawings of the E-Cooler can be found in CDD database under AD_->L→LNTML
A general layout could be this one: [ad_lntml0039](https://edms.cern.ch/ui/file/1560832/0/ad_lntml0039-v0.pdf)

!!!info "References"
    - 2020: *J.R. Hunt et al.*, Novel transverse emittance measurements for electron cooling characterization - [PRAB](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.23.032802)
    - 2019: *J.R. Hunt*, Beam Quality Characterisation and the Optimisation of Next Generation Antimatter Facilities - [PhD thesis](https://livrepository.liverpool.ac.uk/3039086/)
    - IPAC2018: *G. Tranquille et al.*, THE CERN-ELENA ELECTRON COOLER MAGNETIC SYSTEM - [TUPAF056](https://accelconf.web.cern.ch/AccelConf/ipac2018/papers/tupaf056.pdf)
    - 2017: *G. Tranquille*, Status of the ELENA Electron Cooler - [indico](https://indico.cern.ch/event/594422/contributions/2406684/attachments/1396923/2129939/ELENA_Electron_Cooler_Status_ADUC_2017.pdf)
        - Some pictures/detail about ELENA cooler design
        - Details on measurement of magnetic field straightness with plots
        - Characterization of electron gun (e- current vs cathode voltage)
        - Some BETACOOL simulations in backup slides, I guess for the two plateaus
    - IPAC2016: *G. Tranquille et al.*, THE ELENA ELECTRON COOLER - [tupmr006](http://accelconf.web.cern.ch/accelconf/ipac2016/papers/tupmr006.pdf)
        - Main cooler parameters
        - Some word on magnetic measurements and gun/collector design, as well as vacuum parts
    - IPAC2016: *G. Tranquille et al.*, DESIGN AND OPTIMISATION OF THE ELENA ELECTRON COOLER GUN AND COLLECTOR - [thpmb048](http://accelconf.web.cern.ch/accelconf/ipac2016/papers/thpmb048.pdf)
        - Some details on e-cooler design using COMSOL
        - Several plots with expected beam envelop.
    - 2016: *D. Luckin*, Plans by TESLA for magnetic Measurement - [indico](https://indico.cern.ch/event/638834/contributions/2589321/)
    - 2014: *G. Tranquille*, Modeling the ELENA Electron Cooler with COMSOL Multiphysics Software - [link](https://www.comsol.nl/paper/modeling-the-elena-electron-cooler-with-comsol-multiphysics-software-17797)
    
    

    

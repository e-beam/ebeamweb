# Simulation software

## BetaCool 
Is the standard de-facto for e-cooling simulations, developed by JINR:

- Official link: http://betacool.jinr.ru (not accessible) 
- Un-official github repository: https://github.com/dgamba/betacool
- Local CERN mirror repository: https://gitlab.cern.ch/e-beam/betacool

!!!info "Recent publications"
    *   2021: *A. Borucka et al.*, Benchmark of e-cooling simulations with RF-Track, [CERN-ACC-NOTE-2021-0031](https://cds.cern.ch/record/2791367)
    *   2013: *Stoel, L S*, Benchmarking BETACOOL with data from LEIR and AD, [CERN-STUDENTS-Note-2013-145](https://cds.cern.ch/record/1596240)
    *   2012: *A.O.Sidorin et al*, Long Term Beam Dynamics Simulation With The Betacool Code [TUACH02](https://accelconf.web.cern.ch/accelconf/rupac2012/papers/tuach02.pdf)
    *   2006: Physics guide of BETACOOL code version 1.1 - [inspire](http://inspirehep.net/record/741041)
    *   2005: *A.O.Sidorin et al*, BETACOOL program for simulation of beam dynamics in storage rings - [link](https://www.sciencedirect.com/science/article/pii/S0168900205021479?via%3Dihub)
    *   Interesting presentation about BETACOOL development (2003) [BNL](https://www.bnl.gov/cad/ardd/ecooling/docs/PDF/eCoolDynamics/3_Sidorin_BETACOOL.pdf)

## RFTrack
- Developed and maintained by Andrea Latina (CERN).
- Official repository with documentation:
    - [https://gitlab.cern.ch/rf-track](https://gitlab.cern.ch/rf-track)
- Some info available on [twiki](https://twiki.cern.ch/twiki/bin/view/ABPComputing/RF-Track)
- The git repository contains also the reference documentation, some part of which is available in presentations, e.g:
    * [Ecool_and_Elens_2019_01_10.pdf](https://indico.cern.ch/event/774322/contributions/3217585/attachments/1776825/2889336/Ecool_and_Elens_2019_01_10.pdf)
    * [Ebeam_13_Mar_2019.pdf](https://indico.cern.ch/event/802502/contributions/3336299/attachments/1811225/2958382/Ebeam_13_Mar_2019.pdf)

!!!info "Recent publications"
    *   LINAC2016: *A.Latina*, RF-Track: Beam tracking in field maps including space-charge effects, features and benchmarks [CDS](http://cds.cern.ch/record/2304522?ln=it)

## JSPEC:
- Developed and used at JLab
- Official repository with documentation:
    - https://github.com/zhanghe9704/electroncooling
    - https://github.com/JeffersonLab/ElectronCooling
- Online version by Radiasoft LLC: [JSPEC online](https://www.sirepo.com/jspec#)

!!!info "Recent publications"
    * 2019: *H. Zhang et al.* The Latest Code Development Progress of JSPEC, NAPAC2019, [TUPLO04](https://napac2019.vrws.de/papers/tuplo04.pdf)

## XSuite:

- Cooling module not yet available, but plans to have it in 2022 (maybe an integration with Betacool and/or RF-Track)
    - Presently the preferred option for future development of tracking code at CERN
- Online documentation available at [link](https://xsuite.readthedocs.io/en/latest/)

!!!info "Recent publications"
    **2021:**G. Iadarola _et al._* Xsuite code - presentation [indico](https://indico.cern.ch/event/1071000/contributions/4503854/attachments/2304760/3920913/020_Xsuite.pdf)


## TRACKIT:
- Developed at BNL
- Used for bunched beam cooling simulations
    - See for example this [presentation](https://indico.cern.ch/event/792306/contributions/3298802/attachments/1810603/3739539/electron%20cooling.pdf)

!!!info "Recent publications"
    **Jul 2020:** _H. Zhao et al._ Cooling simulation and experimental benchmarking for an rf-based electron cooler [PRL 23.074201](https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.23.074201)



## Other codes/references:

*   VORPAL - "Direct simulation of Friction force based on Molecular Dynamics techniques" [https://oraweb.cern.ch/pls/hhh/code_website.disp_code?code_name=VORPAL](https://oraweb.cern.ch/pls/hhh/code_website.disp_code?code_name=VORPAL)
    *   probably now integrated in some other code...
*   2005: Kinetic Effects in Multiple Intra-Beam Scattering - [https://aip.scitation.org/doi/pdf/10.1063/1.1949577?class=pdf](https://aip.scitation.org/doi/pdf/10.1063/1.1949577?class=pdf)
    *   MOCAC code for Intra-Beam Scattering simulations 
    *   2009: Other interesting reference with some explanations about MOCAC used for CLIC damping rings
        *   [https://indico.cern.ch/event/50306/contributions/1180212/attachments/963456/1367891/CLIC_BeamDynamicsMeeting_09_01_14.pdf](https://indico.cern.ch/event/50306/contributions/1180212/attachments/963456/1367891/CLIC_BeamDynamicsMeeting_09_01_14.pdf)

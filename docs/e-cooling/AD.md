# Antiproton Decelerator (AD)


## The decelerator
Table from [https://indico.cern.ch/event/361413/](https://indico.cern.ch/event/361413/)

<table><colgroup><col> <col> <col></colgroup>
<tbody>
<tr>
<th>Momentum pbar</th>
<th>300 MeV/c</th>
<th>100 MeV/c</th>
</tr>
<tr>
<td>Electron energy</td>
<td>about 27 keV</td>
<td>about 3 keV</td>
</tr>
<tr>
<td>Electron current</td>
<td>about 2.2 A</td>
<td>about 200 mA</td>
</tr>
<tr>
<td>Cooling length</td>
<td colspan="2" style="text-align: center;">1.5 m</td>
</tr>
<tr>
<td>Drift magnet field</td>
<td colspan="2" style="text-align: center;">590 Gauss</td>
</tr>
<tr>
<td>Electron beam radius</td>
<td colspan="2" style="text-align: center;">25 mm</td>
</tr>
<tr>
<td>Cooling time</td>
<td>16 s</td>
<td>15 s</td>
</tr>
<tr>
<td>$\epsilon_x$ / $\epsilon_y$</td>
<td>3 / 3  ($\pi$ × mm × mrad)</td>
<td>0.8 / 0.5 ($\pi$ × mm × mrad)</td>
</tr>
<tr>
<td>dp/p</td>
<td>10-4</td>
<td>< 7 × 10-5</td>
</tr>
</tbody>
</table>

### Technical drawings
They can be found on CDD under AD_ project, under “beecn0” folder.
Of interest is the overall layout also available [here](docs/AD_EC_Layout.pdf)

## AD E-COOLER

### Powering scheme
Detailed electrical schematics available [here](docs/Power_Diagram_AD.pdf) (by A. Frassier)    


## New AD E-COOLER
!!!info "Some links"
    - 14 Oct 2020: New AD Electron Cooler : Kick-off meeting - [indico](https://indico.cern.ch/event/962591/)

## Old AD E-COOLER
!!!info "Some links"
    - **SY-BI repository of documentation on [EDMS](https://edms.cern.ch/project/CERN-0000205848)**
    - 1997: *R. Brown and G. Tranquille*, Magnetic Field Measurements of the LEIR Electron Cooling Device [EDMS #2340163](https://edms.cern.ch/document/2340163/1)
         - Description of magnetic measurements performed on the now-AD electron cooler. The document gives mainly a description of the measurements, but not much info about the actual results.
    - Jun 1984: *A. Wolf*, Magnetic field measurements in the electron cooling device for LEAR - [metadata](https://inis.iaea.org/search/searchsinglerecord.aspx?recordsFor=SingleRecord&RN=15064541) [CDS](https://cds.cern.ch/record/149879?ln=en)

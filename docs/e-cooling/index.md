# E-Cooling

Here you can find a list of useful references and a list of effects one should keep in mind (still to be completed!)
Use the left menu for details on e-coolers in different machines at CERN and on the available software for simulation.

!!!info "Interesting external websites"
    - Summary of measurements performed at TSR [link](https://www.mpi-hd.mpg.de/blaum/storage-rings/tsr/e-cooling.en.html)
        - Good source of ideas on what to do next
    - COOL’21 conference (Novosibirsk) [link](https://indico.inp.nsk.su/event/59/)
    - COOL’19 conference (Novosibirsk) [link](https://indico.inp.nsk.su/event/16/)
    - COOL’17 conference (Bonn) [link](https://indico-jsc.fz-juelich.de/event/48/)
    - COOL’15 conference (JLAB) [link](https://www.jlab.org/conferences/cool15/past.html)
        - with list of previous conferences
    - Wiki space from JLAB about [e-cooling](https://wiki.jlab.org/ciswiki/index.php/Electron_Cooling) and [Electron guns](https://wiki.jlab.org/ciswiki/index.php/Electron_Guns_Overview).


# E-cooling in the world

List of some known e-coolers in the world (to be completed/verified):

|                   | $I_e$ [A]| $e^-$ $E_k$ [keV] | $B_{gun}$ - $B_{drift}$ [G]  | Ions  | In operation  | Reference |
|:-----------------:|:--------:|:-----------:|:-------------:|:----------------:|:-:|:---------------------:|
| AD (CERN)         | <2.5   |  27   | 590 - 590   | $\bar{p}$        | y | [PS-LI-Note-87-6](https://cds.cern.ch/record/679934?ln=en) |
| ELENA (CERN)      | <0.005 | 0.355 | 1000 - 100  | $H^-$, $\bar{p}$ | y | [IPAC2016](https://accelconf.web.cern.ch/ipac2016/papers/tupmr006.pdf) |
| LEIR (CERN)       | 0.6    | 2.3   | 2300 - 750  | $^{208}Pb^{54+}$ | y | [10.1016/j.nima.2004.06.072](https://cds.cern.ch/record/818466?ln=en)
| CELSIUS (Uppsala) | 3      | 300   | 1500 - 1000(?) |                  | n | [10.5170/CERN-1994-003.235](https://cds.cern.ch/record/398603) [ISBN 978-91-554-7871-1](https://www.diva-portal.org/smash/get/diva2:344915/FULLTEXT02)|
| CSR (Heidelberg)  |        | 0.165 | ~50         |                  | y | [COOL2009](https://accelconf.web.cern.ch/COOL2009/papers/thm2mcco03.pdf) |
| TSR (Heidelberg)  |        | 16    | 12000-3000  |                  | n |   |
| NICA Booster (Dubna) | <1 | <60   | 700         | $^{4}He^{1+}$, $^{56}Fe^{14+}$ | y |[RuPAC2021](https://doi.org/10.18429/JACoW-RuPAC2021-TUPSB04) [RuPAC2018](https://accelconf.web.cern.ch/rupac2018/papers/tupsa22.pdf)|
| NICA Collider (Dubna) | 1  | 2450  | 2000 - 500  | $p$, $^{194}Au^{79+}$ | n | [COOL2019](https://doi.org/10.18429/JACoW-COOL2019-THX01) |
| CSRe (Lanzhou)    | 3      | 300   | 5000 - 1500 | $^{12}C^{6+}$    | y | [COOL2009](https://accelconf.web.cern.ch/COOL2009/papers/frm1mcio02.pdf) [NIMA, 2000,441:87](https://doi.org/10.1016/S0168-9002(99)01114-6)|
| CSRm (Lanzhou)    | 3      | 35    | 2400 - 1500 | $^{12}C^{6+}$, $^{36}Ar^{18+}$ | y | [COOL2009](https://accelconf.web.cern.ch/COOL2009/papers/thm1mcco03.pdf)
| HIAF (Lanzhou)    | 2      | 50(450)    | 2500(4000) - 1500 |  |  | [COOL2017](https://inspirehep.net/files/395fc04dfb5b3353dfd82515cd5d458a)
| SIS (Darmstadt)   |        | 35    |             |                  |  | |
| ESR (Darmstadt)   | 1      | 230   | 2000 - 100  |                  |  | |
| HESR (Darmstadt)  |        |       |             |                  | n | |
| CRYRING (Darmstadt)        | 13    |             |                  | y | |
| COSY (Julich)     | 4      | <100   | 1650 - 800  | $p$, $^{2}H^{+}$ | y | [arxiv](https://arxiv.org/pdf/1101.5963.pdf), [1993](https://cds.cern.ch/record/398615/files/p317.pdf), [1992](https://epaper.kek.jp/e92/PDF/EPAC1992_0839.PDF)| 
| COSY (Julich)     | <0.5   | <908   |   | $p$, $^{2}H^{+}$ | y | [arxiv](https://juser.fz-juelich.de/record/187936/files/mopri070.pdf?subformat=pdfa)|
| RHIC-LEReC (BNL)  |        | 2000  |             |                  | y | [COOL2021](https://accelconf.web.cern.ch/cool2021/papers/s601.pdf) |
| IOTA (FNAL)       | 0.01   | 1.36  | 1000        | $p$              | n | [arxiv](https://arxiv.org/pdf/2201.10363.pdf) |
| NAP-M (Novosibirsk)|       |       |             |                  |  | | 
| S-LSR (Kyoto)     |        | 4     |             |                  |  | | 
| TARN II (Tokyo)   |        | 110   |             |                  |  | | 



!!!info "References about Electron Cooling and related topics, in descending chronological order:"
    - **May 2022**: "Magnetic design of the new AD electron cooler (PXMLCEANWC)"  [EDMS](https://edms.cern.ch/document/2731780/1)
    - **Nov 2021**: _D. Gamba et al._ Presentation on AD/ELENA electron cooling experience during and after CERNs Long Shutdown (LS2) [COOL21-S503](https://indico.inp.nsk.su/event/59/attachments/1468/1975/S503_TALK.PDF) [proceedings](https://epaper.kek.jp/cool2021/papers/s503.pdf)
    - **Nov 2021**: _A. Borucka et al._ COMPARISON OF AVAILABLE MODELS OF ELECTRON COOLING AND THEIR IMPLEMENTATIONS, COOL21, [proceedings](https://epaper.kek.jp/cool2021/papers/p1005.pdf)
    - **Aug 2021**: _H. Zhao et al._ Rate redistribution in dispersive electron cooling [PRL 24.083502](https://journals.aps.org/prab/pdf/10.1103/PhysRevAccelBeams.24.083502)
    - **May 2021**: _C. Krantx et al._ Transverse electron cooling of heavy molecular ions [link](https://doi.org/10.1103/PhysRevAccelBeams.24.050101)
        - Interesting source of data points for cooling simulations
        - No much information about cooling time as a function of optics functions
    -  **May 2021**: _G. Stancari et al._ Beam physics research with the IOTA electron lens [link](https://iopscience.iop.org/article/10.1088/1748-0221/16/05/P05002)
        - Detailed description of physics aspect for electron cooling and related at IOTA. Good starting point for physics aspect one should take into account in a low-energy ring with e-cooling-like devices.
    - **Jul 2020:** _H. Zhao et al._ Cooling simulation and experimental benchmarking for an rf-based electron cooler [PRL 23.074201](https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.23.074201)
        - Cooling simulations using `TRACKIT' - an BNL in-house devoloped code for bunched beam cooling simulations 
    - **Nov 2020:** _S. Seletskiy et al._ Obtaining transverse cooling with nonmagnetized electron beam [PRL 23.110101](https://journals.aps.org/prab/pdf/10.1103/PhysRevAccelBeams.23.110101)
        - Experience of high-energy electron cooling (with nonmagnetized electron beam) at RHIC
    - **Nov 2020:** _Markus Steck, Yuri A. Litvinov_ Heavy-ion storage rings and their use in precision experiments with highly charged ions [j.ppnp.2020.103811](https://doi.org/10.1016/j.ppnp.2020.103811)
        - Excellent review of running accelerators and general concepts used (including cooling) with many references
    - **26/03/2019:** AD Cooler review [indico](https://indico.cern.ch/event/797056/)
    - **2018**: _N.S. Dikansky et al._ **Development of techniques for the cooling of ions** [inspire](http://inspirehep.net/record/1680413)
        - Very interesting review (maybe) but **in Russian**...
    - **Fall 2017**: V. N. Litvinenko et al. Hadron Beam Cooling [slides](http://case.physics.stonybrook.edu/images/b/b0/PHY564_Lecture_24_F2017.pdf)
        - Short presentation with main results of cooling (both electron and stochastic)
    - **3/10/2017:** Electron Cooling Studies (LEIR) [indico](https://indico.cern.ch/event/669271/contributions/2736756/)
        - Results from steering test in LEIR electron cooler
        - Big horizontal emittance/cooling variation while scanning in vertical
            - To be continued
    - **7/08/2017:** LEIR activities follow up, [indico](https://indico.cern.ch/event/650328/contributions/2645281/attachments/1504275/2344161/HSC_LEIR_07082017_NB.pdf)
        - status of LEIR multi-injeciton, high intensity.
        - plans to do orbit scan in the cooler.
    - **Jun 2017:** *Andrea Latina* [indico](https://indico.cern.ch/event/640593/contributions/2602193/attachments/1469186/2272657/ABP_Information_01062017.pdf)
        - RF-track approach to e-cooling simulations
    - **2017:** *Yaroslav S. Derbenev* Theory of electron cooling [arxiv](https://arxiv.org/abs/1703.09735)
        - Interesting theory review, translated from Russian 
    - **2016:** *J. Resta-López et al.* Non-Gaussian beam dynamics in low energy antiproton storage rings [link](https://www.sciencedirect.com/science/article/pii/S0168900216308191)
    - **Sep 2015:** *Vsevolod Kamerdzhiev* Introduction to electron cooling [indico](https://indico.cern.ch/event/357886/contributions/849347/attachments/1148562/1647628/EC.pdf)  
        - <span style="color: red;">interesting presentation with theory from 1990 paper I think…</span>
    - **2015**: *J. Resta-López et al.* Simulation studies of the beam cooling process in presence of heating effects in the Extra Low ENergy Antiproton ring (ELENA) [link](http://iopscience.iop.org/article/10.1088/1748-0221/10/05/P05012)
    - **2015**: *Igor Meshkov et al.* Storage and cooling of ion beams [link](https://iopscience.iop.org/article/10.1088/0031-8949/2015/T166/014037/pdf)
        - <span style="color: rgb(255,0,0);">**review**</span> of techniques for accumulation in storage rings, including electron cooling (with references) and stochastic cooling.  
    - **2014**: **<span style="color: rgb(255,0,0);">Beam Dynamics Newsletter no65 on Beam Cooling</span>**s
        - Very broad review of cooling
        - [full text](http://www-bd.fnal.gov/icfabd/Newsletter65.pdf)
    - **July 2014**: _I.Meshkov_ Beam Cooling Techniques [indico](https://indico.cern.ch/event/297045/contributions/1658342/attachments/557291/767841/I_Meshkov_CoolgTechnq.pdf)
        - Very interesting presentations with a lot of details!
    - **Apr 2014**: _Daria Astapovych_ [https://espace.cern.ch/be-dep-workspace/abp/HSC/Meetings/Electron_cooling (2).pdf](https://espace.cern.ch/be-dep-workspace/abp/HSC/Meetings/Electron_cooling%20(2).pdf)
        - Interesting presentation with references
        - Some parts were extracted from _K. Rahsman_ - Modeling of Electron Cooling (2010) - [thesis](https://www.diva-portal.org/smash/get/diva2:344915/FULLTEXT02)
    - **SAP2014:** _L.J. Mao et al._, Electron cooling experiments at HIRFL-CSR ([link](http://accelconf.web.cern.ch/AccelConf/SAP2014/papers/thpmh2.pdf))
        - Interesting measurements of dragging force as done at LEIR
    - **PRSTAB 2013**: _Hrachya B. Nersisyan et al._, Cooling force on ions in a magnetized electron plasma [PhysRevSTAB.16.074201](https://journals.aps.org/prab/pdf/10.1103/PhysRevSTAB.16.074201)
    - **IPAC2012**: _V.V. Parkhomchuk_, Development of electron coolers in Novosibirsk [WEXA02](https://accelconf.web.cern.ch/IPAC2012/papers/wexa02.pdf)
        - Review of the e-cooler technology used by BINP team to build their e-coolers, from NAP-M to LEIR to COSY.
        - Shows picture of Andrey Sery measuring cooling force on H- and H+. 
        - Also stating that one should maximise magnetic field for having strongest cooling force!
    - **RUPAC2012**: _M. Bryzgunov et al._, HIGH VOLTAGE ELECTRON COOLER [TUXCH01](https://accelconf.web.cern.ch/rupac2012/papers/tuxch01.pdf)
        - Discussion on physical requirements for 2 MeV e-cooler for COSY.
        - Magnetic field straightness requirement of better than 1e-5. 
        - Showing gun design with grid split in 4 to allow for measuring space charge effect (and therefore neutralisation) on e- beam transport
    - **2011**: _H. J. Stein et al._ Present Performance of Electron Cooling at Cosy-Julich [arxiv](https://arxiv.org/abs/1101.5963)
        - Status of COSY e-cooling (similar to AD e-cooler).
        - Despite very good magnetic field quality measured in 1993 (see referecens below), in this paper it is quoted that cooling force was more inline to **nonmagnetized** coling.
    - **COOL2009**: _L.J.Mao et al._ COOLING FORCE MEASUREMENTS WITH VARIABLE PROFILE ELECTRON BEAM AT HIRFL-CSR [THM1MCCO03](https://accelconf.web.cern.ch/COOL2009/papers/thm1mcco03.pdf)
i       - Measurement of cooling force using CSR Main ring with e-cooler similar to the one of AD, but gun similar to the one of LEIR (multiple profiles measured)
        - Estimate of magnetic field quality from cooling force measurement down to 8e-5 (using formula of Parkhomchuk’s semi-empirical formula, with details!)
        - Measurement compatible field quality measurement in "Test Results of HIRFL-CSR Main Ring Electron Cooling Device" (2003, [Chinese Physics C](http://zgwlc.xml-journal.net/article/id/b27c9fe0-f6c3-4993-b06d-fe87e2a59e24)
    - **CAS2008**: _F. Caspers_, Lecture on Schottky Signals ([link](https://cas.web.cern.ch/sites/cas.web.cern.ch/files/lectures/dourdan-2008/caspers.pdf))
    - **COOL2007:** Cooling Results from LEIR, [link](http://accelconf.web.cern.ch/AccelConf/cl07/papers/tum1i01.pdf)
        - (to be read)
    - **PhysRevSTAB 2006** _A. V. Fedotov et al._ Numerical study of the magnetized friction force, [link](http://dx.doi.org/10.1103/PhysRevSTAB.9.074401)
        - Overview of different approaches to simulate friction force
        - **First brute force approach to simulate cooling force using VORPAL**
    - **EPAC06:** LEIR ELECTRON COOLER STATUS, [link](https://cds.cern.ch/record/972691/files/lhc-project-report-944.pdf)
        - Nice overview of the commissioning of LEIR e-cooler
        - (to be read)
    - **2006:** _A. V. Fedotov et al._ Experimental studies of the magnetized friction force [PRE73](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.73.066503)
        - Report on experiments in CELSIUS aimed at a detailed study of the magnetized friction force.
    - **2006:** _A.O. Sidorin et al._ BETACOOL program for simulation of beam dynamics in storage rings [link](http://inspirehep.net/record/719210)
    - **2006:** _Valentin Bocharov et al._ First Tests of LEIR - Cooler at BINP [DOI](http://dx.doi.org/10.1063/1.2190134)
        - Report by BINP with measurements of magnetic field and electron currents achieved
    - **2006:** _Valentin Bocharov et al._ Precise Measurements of a Magnetic Field at the Solenoids for Low Energy Coolers [doi](https://doi.org/10.1063/1.2190135)
        - Details on the design of the pancake structure of LEIR ecooler and measurement of magnetic field.
    - **2006:** _V. V. Parkhomchuk_ Development of a New Generation of Coolers with a Hollow Electron Beam and Electrostatic Bending [doi](https://doi.org/10.1063/1.2190119)
        - Details on the physics of using electrostatic bending
        - Also details on definition of effective velocity taking into account magnetic field straightness!
        - Also details on the gun design with variable profile
    - **31/05/2005:** _Hakan Danared_ Beam Cooling - CAS - [link](https://cas.web.cern.ch/sites/cas.web.cern.ch/files/lectures/zeegse-2005/cas05c-2.pdf)
        - Some detailed derivation of friction force
        - There are also proceedings [CDS](http://cds.cern.ch/record/813710/)
        - Link to possibility to measure the electron temperatures
    - **15/09/2004:** _G. Tranquille_ Specification of a new electron cooler for the low energy ion accumulator ring, LEIR. [link](https://www.sciencedirect.com/science/article/pii/S016890020401277X?via%3Dihub)
    - **11/10/2004:** _G. Tranquille_ Recent highlights from the CERN-AD [NiMA](https://doi.org/10.1016/j.nima.2004.06.036)
        - General report with serveral e-cooler (but not only!) tests in AD (neutralisation, dispersion in the e-cooler, ...)
        - Provides a table with performance reached in AD
    - **Oct. 2004:** _M.Nishiura et al._ Simulation studies of the electron cooler for MUSES at RIKEN [link](https://www.sciencedirect.com/science/article/pii/S0168900204012793)
        - Study of electron trajectory into toroids of e-cooling. 
    - **Sep. 2004:** _J. Bosser et al._ Optimum lattice functions for electron cooling [link](https://doi.org/10.1016/j.nima.2004.06.075)
        - Summary of previous reports on Optimum Parameters for Electron Cooling
        - It contains some attempt to demonstrate reasons for faster cooling with dispersion
    - **2003:** _G. Tranquille_ Diagnostics for Electron Cooled Beams [link](https://cds.cern.ch/record/624193)
        - Overview of different instruments to measure e- and ions (Schottky, IPM, screen, Faraday Cup, neutrals monitor, laser, antenna, ...)
    - **2003:** _M. Beutelspacher et al._ Dispersive electron cooling experiments at the heavy ion storage ring TSR [link](https://doi.org/10.1016/S0168-9002(03)01976-4)
        - Observation of better cooling at TSR with D=1.8 m. (Note that beam parameters probably close to CERN coolers)
        - Some theory to attempt to describe the effect
    - **2001:** _G. Tranquille_ Optimum Parameters for Electron Cooling [cds](https://cds.cern.ch/record/518755)
    - **2001:** _A. Skrinsky_ ELECTRON COOLING and ELECTRON-NUCLEI COLLIDERS [link](http://www.slac.stanford.edu/econf/C010630/papers/T903.PDF)
        - Some math of e-cooling
    - **2001:** _V.V. Parkhomchuk_ Electron Cooling for RHIC [link](https://www.bnl.gov/isd/documents/79867.pdf)
    - **2000:** _V.V. Parkhomchuk, Aleksandr N Skrinskii_ Electron cooling: 35 years of development [link](https://iopscience.iop.org/article/10.1070/PU2000v043n05ABEH000741)
        - Interesting review of Parkhomchuk's cooling force and details
    - **2000:** _V.V. Parkhomchuk_ New insights in the theory of electron cooling [link](https://www.sciencedirect.com/science/article/pii/S0168900299011006?via%3Dihub)
        - Reference of Parkhomchuk semi-empirical model of cooling force
    - **2000:** _Bosser, J. et al._ Stability of cooled beams, [NIMA](https://cds.cern.ch/record/392306)
        - Review of previous studies in LEAR and recomandation for controlling neutralisation and impedance
    - **2000** _N St J Braithwaite_, Introduction to gas discharges [link1](https://iopscience.iop.org/article/10.1088/0963-0252/9/4/307/pdf) [link2](http://home.sandiego.edu/~severn/intro_gas_discharges.pdf)
        - Interesting summary with physics about particle interaction in plasmas.
    - **1999** _Bosser, Jacques et al._ Stability of cooled beams [NIMA](https://cds.cern.ch/record/392306)
        - Review 
    - **1999** _Bosser, Jacques et al._ On the Optimum Dispersion of a Storage Ring for Electron Cooling with High Space Charge [CERN-PS-99-045-OP](http://cds.cern.ch/record/394167)
    - **1999** _Bosser, Jacques et al._ Experimental investigation of electron cooling and stacking of lead ions in a low energy accumulation ring [CERN-PS-99-033-DI](http://cds.cern.ch/record/388111)
        - Some generic formula for cooling time
        - **Main summary of cooling experiments done at LEAR (with the AD-like cooler)**
        - Includes data from cooling time as a function of optics parameters in the cooler
    - **1999** _N. Madsen_, Simulations of Electron Cooling Using BETACOOL [link](https://www.researchgate.net/publication/237519061_SIMULATIONS_OF_ELECTRON_COOLING_USING_%27BETACOOL)
        - Comparison of LEAR cooler performance and extrapolation to AD. Very extensive study!
    - **1998** _S. Mannervik et al._, Strong Relativistic Effects and Natural Line widths Observed in Dielectronic Recombination of Lithiumlike Carbon [link](https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.81.313)
        - Measurement of electron temperatures using dielectronic recombination spectrum of $C^{3+}$.
    - **1997:** _J.Bosser et al._ Experimental studies of electron beam neutralisation [link](https://doi.org/10.1016/S0168-9002(96)01192-8)
        - Details on measurement of e- neutralisation in LEAR
        - See also earlier paper of 1995 by J.Bosser on this topic
    - **1997:** _I.Meshkov_ Electron cooling — the first 30 years and thereafter [link](https://doi.org/10.1016/S0168-9002(97)00277-5)
        - interesting overview. Some formulas (e.g. cooling time).
    - **1996:** _Bosser, Jacques_ Review of recent work on electron cooling at LEAR [CERN-PS-96-013-AR](https://cds.cern.ch/record/301831?ln=en)
        - This paper describes the measurements made on the cooling and lifetimes and on the neutralization technique implemented on the LEAR electron cooler.
        - Also reports on lifetime of different Pb charge states, see paper from 1995 below
    - **1995:** _Baird, S A  et al._ Measurement of the lifetime of $Pb^{52+}$, $Pb^{53+}$ and $Pb^{54+}$ beams at 4.2 MeV per nucleon subject to electron cooling [CERN-PS-95-35-AR](https://cds.cern.ch/record/290100?ln=en)
    - **1995:** _Bosser, J. et al_ Neutralisation of the LEAR electron-cooling beam: experimental results [DOI](http://dx.doi.org/10.1109/PAC.1995.505745)
        - Details about how neutralisation was implemented/studied in LEAR e-cooler
    - **1995:** _Bosser, Jacques_  Electron cooling (storage rings), CAS School [10.5170/CERN-1995-006.673](https://cds.cern.ch/record/302476?ln=en)
        - Refers to e-cooling physics by Helmut Poth (see ~1990)
        - List of effects on the circulating beam (instabilities, kicks due to toroids, rest-gas interaction)
        - Also provides a table with existing e-cooler at the time
    - **1994:** _I.Meshkov_ Electron Cooling Status and Perspectives, Physics of Particles and Nuclei [link](http://inspirehep.net/record/389221) [linkCDS](https://cds.cern.ch/record/398450?ln=en)
        - theory explained, but not fully...
    - **1993:** _Prasuhn, D et al._ Technical Features and Final Electron Beam Tests of the COSY Electron Cooler and First Proton Beam Cooling [cds](https://cds.cern.ch/record/398615/)
        - Details about COSY e-cooler (very similar to AD e-cooler in some ways). Magnetic field quality measured at the the level of 10^-5
    - **1993:** _Bosser, Jacques (ed.)_ Workshop on Beam Cooling and Related Topics [CDS](https://cds.cern.ch/record/254427/)
        - General overview of all cooling techniques and experience at the time (stochastic cooling, laser cooling, electron cooling, muon cooling...)
        - theory of e-cooling by _I. Meshkov_
        - Interesting details of longitudinal Schottky and Impedance effects (double peak distribution)
        - Several experiences, also on hardware part designs, for e-cooling in different labs
    - **1993:** _Burov, A.V  et al._ Experimental Investigation of an Electron Beam in Compensated State [CDS](https://cds.cern.ch/record/2831342?ln=en)
        - Very detailed study at NAP-M of e- space charge compensation with ions and impact on e- stability
    - **Sep 1992:** _S.P. Moller_ Cooling Techniques [link](https://cds.cern.ch/record/398431?ln=it)
        - CAS school lecture
    - **Feb 1990:** _Helmut Poth_ Further results and evaluation of electron cooling experiments at LEAR [link](https://doi.org/10.1016/0168-9002(90)91818-V) 
    - **Nov 1990:** _Helmut Poth_ Electron cooling: Theory, experiment, application [link](https://www.sciencedirect.com/science/article/pii/0370157390900409)
        - Used as reference in Gerard's 40 years of cooling presentation
    - **1989**: _R. Calabrese et al._ A VERY COLD, LASER INDUCED ELECTRON BEAM FOR STUDIES ON THE FRICTION FORCE IN AN ELECTRON COOLING EXPERIMENT [link](https://inspirehep.net/literature/281715)
    - **1989**: _K. Schindl_ Space charge Theory [link](https://acceleratorinstitute.web.cern.ch/acceleratorinstitute/ACINST89/Schindl_Space_Charge.pdf)
        - CAS school proceedings
    - **1988**: _N. S. Dikansky et al._ Influence on the Sign of an Ion Charge on Friction Force at Electron Cooling [EPAC88](https://accelconf.web.cern.ch/e88/PDF/EPAC1988_0529.PDF)
    - **1987**: _A. Wolf_, "Electron cooling of stored ions", [link](http://cds.cern.ch/record/181046)
    - **1987**: _C. Habfast et al._ "Status report on the LEAR electron cooler", [link](https://cds.cern.ch/record/679934?ln=en) PS-LI-Note-87-6
        - Details on e-cooling adaptation for LEAR, including **measurement of e- temperatures!**
    - **1985**: _H. Poth_ at CAS1985, "Electron cooling theory", [CERN-87-03-V2](http://cds.cern.ch/record/179298/files/CERN-87-03-V2.pdf?version=1) page 534.
        - Details of cooling theory with many details and passages
    - **Dec 1982:** _V. I. Kudelainen et al._ Temperature relaxation in a magnetized electron beam [link](http://www.jetp.ac.ru/cgi-bin/dn/e_056_06_1191.pdf) 
        - Study of perturbation of temperatures of electron beam along the cooler.
    - **1978:** _Derbenev, Yaroslav S; Skrinsky, A N_ The Effect Of An Accompanying Magnetic Field On Electron cooling [Part. Accel. 8 (1978) pp.235-243](https://cds.cern.ch/record/1107957)
        - Detaild theory of cooling force at that time with estimates on the effect of magnetic field line straightness
    - **May 1967:** _G. I. Budker_ An effective method of damping particle oscillations in proton and antiproton storage rings [link](https://link.springer.com/article/10.1007/BF01175204)


# Some theory

A great reference with detailed derivations of physics around e-cooling and e-cooler is **Nov 1990:** _Helmut Poth_ Electron cooling: Theory, experiment, application [CERN-EP/90-04](https://doi.org/10.1016/0370-1573(90)90040-9)

## Effects on electrons

### Perveance limit of gun

### Space charge in e-: velocity distribution

### Temperatures after acceleration

### Transverse drift in toroids

!!!info 
    Nice derivations in [Introduction to plasma physics](https://ocw.mit.edu/courses/nuclear-engineering/22-611j-introduction-to-plasma-physics-i-fall-2003/lecture-notes/chap2.pdf)
In a toroidal field the $e^-$ follow the B field lines. However, due to Centrifugal force, $e^-$ are also subject to a lateral drift with speed given by:

$$
\vec{v}_d = \frac{m v_\parallel^2}{q} \frac{\vec{B} \times \vec{R_C}}{|B|^2 |R_C|^2} 
$$

Assuming a toroid where $R_C$ = $R_{tor}$ is perpendicular to $B$ and $v_\parallel = \beta c$ and $L_{tor} = R_C \Theta_{tor}$:
$$
\Delta X = \frac{m}{q} (\beta c)^2 \frac{1}{B R_C} \frac{L_{tor}}{\beta c} = \frac{m}{q} (\beta c) \frac{\Theta_{tor}}{B}
$$

Then knowing that for electron $\frac{q}{m} = −1.759e11$ [C kg$^{-1}$]; For the **ELENA** e-cooler the lateral offset of the electrons is:
$$
\begin{align}
\Delta X_{ELENA} &= −1/1.759e11 [kg/C] * 0.0146 * 3e8 [m/s] * 3.14/2 * 1/0.01 [C s / kg] \\\
&= - 3.9 \mathrm{ mm}
\end{align}
$$
at 100~keV, and, since it scales with $\beta$, it is about 10 mm for the mid plateau at 653~keV.



### Effect of B field imperfection

## Effects on ions

### kick due to toroidal field
Considering derivation form _Helmut Poth_ and its ![toroid schematics](img/toroid.png), The total the ion beam path, $S=r_{tor} \tan \phi_0$. 
Therefore $\frac{dS}{d\phi} = \frac{r}{\cos^2 \phi}$, while $B_y(\phi) = B_{tor} \sin \phi$. 
Integrating over the whole path, i.e. from $\theta_0$ to zero, one obtains:
$$
\begin{align}
\theta_{x} &= (\cos^{-1} (\phi_0) - 1) \frac{B_{tor} r_{tor}}{B \rho}
\end{align}
$$
For the AD e-cooler ($B_{tor} = 620$ Gauss at 400 A, $\phi_0=0.628319$ rad, $r_{tor} = 1.133$ m), one obtains: $\frac{.00497}{p}$ where $p$ is in units of \[GeV/c\], i.e. up to 50 mrad at 100 MeV/c.

In a similar way, one could compute equivalent integrated solenoidal field of a toroid. The integral involves integrating $1/\cos \phi$ which is equal to $\ln |\sec \phi + \tan \phi| + C$, therefore
$$
\begin{align}
B_s L &= \frac{B_{tor} r_{tor}} * \ln |\sec \phi_0 + \tan \phi_0| 
\end{align}
$$
For AD e-cooler ($B_{tor} = 620$ Gauss at 400 A, $\phi_0=0.628319$ rad, $r_{tor} = 1.133$ m) one obtains 474 Gm to be compared to about 410 Gm in [EDMS 2340163](https://edms.cern.ch/document/2340163/1).

# Low Energy Ion Ring (LEIR)

LEIR is meant to stack several (typically 7) $^{208}Pb^{54+}$ (about 206.5 times greater mass than the proton) injections at 4.2 MeV/u to 72 MeV/u. 


!!!info "References"
    - **SY-BI EDMS repository [EDMS](https://edms.cern.ch/project/CERN-0000205847)**
    - **2020** D.Gamba et al, Discussion on LEIR cooler diagnostics, [indico](https://indico.cern.ch/event/971104/)
    - **2018** G.Tranquille, 40 Years of Electron Cooling at CERN, IPAC2018 [MOZGBF3](https://inspirehep.net/literature/1691302)
        - It contains several references
    - **2018** O. Marqversen, LEIR Schottky measurements and a proposal for using a distributed BPM system for Schottky measurements in ELENA, [indico](https://indico.cern.ch/event/705430/contributions/2895509/attachments/1649538/2637512/ARIES_Schottky_LEIR_ELENA.pdf)
    - **2016** LHC Injectors Upgrade - Vol 2 - Ions [CERN-ACC-2016-0041](https://cds.cern.ch/record/2153863/files/CERN-ACC-2016-0041.pdf)
    - **2008** G.Tranquille et al., ELECTRON COOLING EXPERIMENTS AT LEIR, 2008, [LHC-PROJECT-REPORT-1111](http://cds.cern.ch/record/1123678/files/LHC-PROJECT-REPORT-1111.pdf)
        - Extensive overview of measurements of cooling performance with different settings of the e-cooler
    - **2007** G.Tranquille, Cooling Results from LEIR, COOL2007 [TUM1I01](https://accelconf.web.cern.ch/cl07/papers/tum1i01.pdf)
        - Several results, most of which also presented in 2008 review by Gerard (LHC-PROJECT-REPORT-1111)
    - **2006** G.Tranquille et al., LEIR ELECTRON COOLER STATUS, 2006, [lhc-project-report-944](http://cds.cern.ch/record/972691/files/lhc-project-report-944.pdf)
	- First report on e- beam current as a function o grid and control 
        - Some details about magnetic field correction
    - **2006** G.Tranquille et al., COMMISSIONING OF THE LEIR ELECTRON COOLER WITH Pb+54 IONS, rupac2006, [webo01](http://rupac2006.inp.nsk.su/ready/webo01.pdf)
        - Several details about commissioning (e.g. issue with vacuum?!), with some references.
        - It states that pancake solenoids allow to achieve better than $B_\perp/B < 10^{-4}$.
    - **2008** G.Tranquille et al., COOLED BEAM DIAGNOSTICS ON LEIR, EPAC08, [TUPC102](https://pdfs.semanticscholar.org/6bde/b66763d41329dac814290018be57dbd0b09d.pdf)
    - **2006** Valentin Bocharov et al., First Tests of LEIR — Cooler at BINP, 2006, [COOL2006](https://doi.org/10.1063/1.2190134)
        - Several details, not found elsewhere
    - **2004** G.Tranquille, Specification of a new electron cooler for the low energy ion accumulator ring, LEIR [NIMA.2004.06.072](https://doi.org/10.1016/j.nima.2004.06.072)
    - **2002** Feasibility study for the design, construction and testing of an electron cooler for LEIR (CERN), [EDMS #2323592](https://edms.cern.ch/ui/file/2323592/1/LEIR_ECOOL_Design_BINP_doc_cpdf.pdf)
        - Details of implementation of LEIR e-cooler, including derivation of forces/effects/cooling times... 
    - **2002** THE ELECTRON GUN WITH VARIABLE BEAM PROFILE FOR OPTIMIZATION OF ELECTRON COOLING, EPAC2002, [WEPRI049](https://accelconf.web.cern.ch/e02/PAPERS/WEPRI049.pdf)
        - Some details about e- gun designed for CSR with ULTRASAM/measurement of beam profile. Same gun should be in LEIR
    - **1997** S.A.Baird et al., Recent Results on Lead-Ion Accumulation in LEAR for the LHC. [CERN-PS-97-003-DI](https://cds.cern.ch/record/319829)
        - Details about lifetime measurement using Pb52+ Pb53+ Pb54+ Pb55+ as well as e-cooler e- beam neutralisation
    - **1996** J. Bosser, Review of recente work on electron cooling at LEAR [CERN-PS-97-037-HP](https://cds.cern.ch/record/319829/files/ps-97-003.pdf) 
        - Details about lifetime measurement using Pb52+ Pb53+ Pb54+ Pb55+ as well as e-cooler e- beam neutralisation
    - **1993** P. Lefevre, and D. Mohl, A low energy accumulation ring of ions for LHC (a feasibility study), [CERN-LHC-Note-259](https://cds.cern.ch/record/279362?ln=en)
        - Contains several details about the machine, but also formulas and estimates for cooling times etc...
    

## LEIR E-COOLER

- The LEIR cooler e- gun is immersed in a high magnetic field (up to about 2.3 kGs or 0.23 T), while the drift is at about 750 Gs (0.075 T), giving the possibility to arrive to expansion factor of about 3, which corresponds to $\sqrt{3}$ max increase in with respect to the cathode radius of about 14 mm, i.e. up to about 24 mm of e-beam radius for k=3.

### Gun settings
See G. Tranquille notes:
![](img/LEIRcurrents.jpg)

### Powering scheme
Detailed electrical schematics available [here](docs/Power_Diagram_LEIR.pdf) (by A. Frassier) 

### E-cooler parameters

| Parameter      | Value                          |
| ----------- | ------------------------------------ |
| `(Main) Ion particle`         | $^{208}Pb^{54+}$ |
| `Momentum`                    | ~88 MeV/c/u |
| `Electron energy`             | 2.3 keV (<6.5keV) |
| `Relativistic beta`           | 0.094 |
| `Electron current`            | 600 mA |
| `Cooler length`               | 2.5 m |
| `Ring length`                 | 78.54 m (about $25\pi$)|
| `Gun magnet field`            | Up to 2.3 kG |
| `Drift magnet field`          | 750 G |
| `Electron beam radius`        | 14-25 mm |


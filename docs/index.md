# E-Beam website
This website is a collection of information about **e-coolers** and **e-lenses** devices at CERN.
It is part of the **E-Beam Working Group** effort for documenting and disseminate the knowledge acquired on those devices.

## E-Beam mandate

The **E-BEAM Working Group** is a forum to:

* **Facilitate exchange** of information amongst all actors involved in electron beam devices.
* Openly **discuss** any matter related to the design and operation of electron beam devices.
* **Review** specifications and needs on electron beam and help translating them into technical requirements.
* **Show operational experience and limitations** of electron devices, express needs for operation and help improving performance where possible.
* **Simulate electron beam** to predict and improve the electron beam dynamics
* **Simulate beam dynamics** in the presence of electron beams to predict cooling or cleaning efficiency.
* Develop and discuss on **simulation tools** and exchange expertise.

## E-Beam meetings

* The official indico space is available [here](https://indico.cern.ch/category/11128/).
* The list of previous (and possible future) meetings is available [here](WGmeetings.md).

!!! info
    The E-Beam meetings are public: Please, feel free to subscribe to the [E-BEAM e-group](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10321679) to be informed about our next meetings.
